#include "accountmanager.h"

namespace Michabo {

AccountManager::AccountManager(QObject *parent) :
    QObject(parent),
    m_selected_account(nullptr)
{
    QSettings settings;
    loadFromSettings(settings);
}

AccountManager::~AccountManager()
{
    // before destroying the world, dump everything to QSettings
    QSettings settings;
    writeToSettings(settings);

    for (auto a : m_accounts)
        delete a;

    m_accounts.clear();
}

bool AccountManager::hasAccounts()
{
    return m_accounts.size() > 0;
}

void AccountManager::addAccount(Account *account)
{
    m_accounts.append(account);

    emit accountAdded(account);
    selectAccount(account);

    QObject::connect(account, &Account::identityChanged, this, &AccountManager::childIdentityChanged);
    QObject::connect(account, &Account::fetchedTimeline, [=] (QString original_name, QList<std::shared_ptr<Post>> posts) {
        emit fetchedTimeline(account, original_name, posts);
    });
    QObject::connect(account, &Account::invalidated, [=] () {
        emit invalidated(account);
    });
    QObject::connect(account, &Account::fetchedInstanceMetadata, [=] () {
        emit fetchedInstanceMetadata(account);
    });
    QObject::connect(account, &Account::invalidatedPost, [=] (Post *p) {
        emit invalidatedPost(account, p);
    });
    QObject::connect(account, &Account::notification, [=] (std::shared_ptr<Notification> n) {
        emit notification(account, n);
    });
}

void AccountManager::childIdentityChanged(Account *account)
{
    emit identityChanged(account);
}

void AccountManager::removeAccount(Account *account)
{
    m_accounts.removeOne(account);

    emit accountRemoved(account);
}

void AccountManager::selectAccount(Account *account)
{
    if (! m_accounts.contains(account))
    {
        qDebug() << "WTF: attempt to select unmanaged account" << account;
        return;
    }

    m_selected_account = account;

    emit accountSelected(account);
}

Account *AccountManager::selectedAccount()
{
    return m_selected_account;
}

void AccountManager::writeToSettings(QSettings &settings)
{
    settings.beginGroup("accounts");

    for (auto a : m_accounts)
        a->writeToSettings(settings);

    settings.endGroup();
}

void AccountManager::loadFromSettings(QSettings &settings)
{
    qDebug() << "Loading any accounts from settings.";

    settings.beginGroup("accounts");

    for (auto child : settings.childGroups())
    {
        settings.beginGroup(child);

        auto account = new Account(settings);
        addAccount(account);

        qDebug() << "Loaded from settings:" << account;

        settings.endGroup();
    }

    settings.endGroup();
}

} // namespace Michabo
