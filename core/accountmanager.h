#ifndef ACCOUNTMANAGER_H
#define ACCOUNTMANAGER_H

#include <QObject>
#include <QSettings>

#include "account.h"
#include "post.h"

namespace Michabo {

class AccountManager : public QObject
{
    Q_OBJECT
public:
    explicit AccountManager(QObject *parent = nullptr);
    virtual ~AccountManager();

    void loadFromSettings(QSettings &settings);
    void writeToSettings(QSettings &settings);

    bool hasAccounts();
    void addAccount(Account *account);
    void removeAccount(Account *account);

    void selectAccount(Account *account);
    Account *selectedAccount();

signals:
    void accountAdded(Account *account);
    void accountRemoved(Account *account);
    void accountSelected(Account *account);
    void identityChanged(Account *account);
    void fetchedTimeline(Account *account, QString original_name, QList<std::shared_ptr<Post>> posts);
    void invalidated(Account *account);
    void fetchedInstanceMetadata(Account *account);
    void invalidatedPost(Account *account, Post *post);
    void notification(Account *account, std::shared_ptr<Notification> n);

public slots:
    void childIdentityChanged(Account *account);

private:
    QList<Account *> m_accounts;
    Account *m_selected_account;
};

} // namespace Michabo

#endif // ACCOUNTMANAGER_H
