#include "aboutwindow.h"

#include <QIcon>
#include <QApplication>

#define LOGO_WIDTH      386 / 2
#define LOGO_HEIGHT     250 / 2

AboutWindow::AboutWindow(QWidget * parent)
    : QDialog(parent),
      m_layout(new QVBoxLayout(this)),
      m_logo(new QLabel),
      m_application_name(new QLabel),
      m_copyright(new QLabel),
      m_license(new QLabel)
{
    QIcon logo = QIcon (":/embedded-assets/icons/banner-colorbg.png");

    m_logo->setPixmap (logo.pixmap (LOGO_WIDTH, LOGO_HEIGHT));
    m_logo->setAlignment (Qt::AlignCenter);

    m_application_name->setText (QString ("<big>%1 %2</big>").arg (qApp->applicationName (),
                                                                   qApp->applicationVersion ()));
    m_application_name->setAlignment (Qt::AlignCenter);

    m_copyright->setText (tr ("Copyright © 2019 Pleroma Contributors"));
    m_copyright->setAlignment (Qt::AlignCenter);

    m_license->setText (tr ("This software is licensed to you under the GNU GPLv3.\n"
                            "A copy of the GPLv3 has been included with the software in the GPL-3 file.\n"
                            "A list of copyright holders has also been included as the AUTHORS file."));
    m_license->setAlignment (Qt::AlignCenter);

    m_layout->addWidget (m_logo);
    m_layout->addWidget (m_application_name);
    m_layout->addWidget (m_copyright);
    m_layout->addWidget (m_license);
}
