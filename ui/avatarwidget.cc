#include "avatarwidget.h"

#include <QPainter>

#include <QDebug>

#define AVATAR_SIZE 48

AvatarWidget::AvatarWidget(QWidget *parent) : QWidget(parent)
{
}

void AvatarWidget::setAvatar(const QImage &new_avi)
{
    avatar = QPixmap::fromImage(new_avi);
    update();
}

QSize AvatarWidget::sizeHint() const
{
   return QSize(AVATAR_SIZE, AVATAR_SIZE);
}

void AvatarWidget::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event)

    QPainter painter(this);
    painter.setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform, true);
    painter.drawPixmap(QRect(QPoint(0,0), size()), avatar,  avatar.rect());
}
