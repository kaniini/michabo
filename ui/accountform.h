#ifndef ACCOUNTFORM_H
#define ACCOUNTFORM_H

#include <QWidget>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QDialogButtonBox>
#include <QVBoxLayout>
#include <QTimer>

#include "core/account.h"
#include "core/accountmanager.h"

class AccountForm : public QWidget
{
    Q_OBJECT
public:
    explicit AccountForm(Michabo::Account *account, QWidget *parent = nullptr);

signals:
    void accountSaved(Michabo::Account *account);
    void accountRejected(Michabo::Account *account);

public slots:

private:
    Michabo::Account *m_account;
    QLabel *m_header;
    QLabel *m_instructions;
    QLineEdit *m_name;
    QLineEdit *m_instance_uri;
    QPushButton *m_auth_button;
    QLineEdit *m_token;
    QDialogButtonBox *m_buttonbox;
    QPushButton *m_save_button;
    QPushButton *m_cancel_button;
    QVBoxLayout *m_layout;
    QTimer *m_instance_uri_timer;
};

#endif // ACCOUNTFORM_H
