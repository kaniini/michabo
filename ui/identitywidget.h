#ifndef IDENTITYWIDGET_H
#define IDENTITYWIDGET_H

#include <QWidget>
#include <QLabel>
#include <QHBoxLayout>
#include <QVBoxLayout>

#include "core/accountmanager.h"
#include "avatarwidget.h"

class IdentityWidget : public QWidget
{
    Q_OBJECT
public:
    explicit IdentityWidget(Michabo::AccountManager *manager, QWidget *parent = nullptr);

signals:

public slots:
    void accountSelected(Michabo::Account *account);
    void updateIdentity(const Michabo::Identity &identity);

private:
    Michabo::AccountManager *m_manager;
    Michabo::Account *m_account;

    QLabel *m_display_name;
    QLabel *m_acct;
    AvatarWidget *m_avatar;
    QHBoxLayout *m_hbox_layout;
    QVBoxLayout *m_vbox_layout;
};

#endif // IDENTITYWIDGET_H
