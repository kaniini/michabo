#include "addaccountbutton.h"
#include "accountform.h"

AddAccountButton::AddAccountButton(Michabo::AccountManager *manager, QWidget *parent)
    : QPushButton(parent),
      m_manager(manager)
{
    QObject::connect(this, &AddAccountButton::clicked, [=] () {
        auto account = new Michabo::Account("", "");
        auto accountform = new AccountForm(account);

        QObject::connect(accountform, &AccountForm::accountSaved, [=] () {
            m_manager->addAccount(account);
        });

        QObject::connect(accountform, &AccountForm::accountRejected, [=] () {
            delete account;
        });

        accountform->show();
    });
}
