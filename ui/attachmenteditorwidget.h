#ifndef ATTACHMENTEDITORWIDGET_H
#define ATTACHMENTEDITORWIDGET_H

#include <QWidget>
#include <QTreeView>
#include <memory>

#include "core/post.h"

class AttachmentEditorModel : public QAbstractListModel
{
public:
    enum {
        Preview,
        Description,
        NColumns
    };

    AttachmentEditorModel(std::shared_ptr<Michabo::Post> scratch);

    int columnCount(const QModelIndex &parent) const;
    int rowCount(const QModelIndex &parent) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role);

    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;

private:
    std::shared_ptr<Michabo::Post> m_scratch;
};

class AttachmentEditorWidget : public QTreeView
{
    Q_OBJECT

public:
    AttachmentEditorWidget(std::shared_ptr<Michabo::Post> scratch, QWidget *parent = nullptr);

    void updateDirty();

private:
    AttachmentEditorModel *m_model;
};

#endif // ATTACHMENTEDITORWIDGET_H
