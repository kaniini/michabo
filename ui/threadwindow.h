#ifndef THREADWINDOW_H
#define THREADWINDOW_H

#include <QWidget>
#include <QVBoxLayout>

#include "timelinewidget.h"

class ThreadModel : public TimelineModel
{
public:
    ThreadModel(Michabo::AccountManager *manager, QString id, QObject *parent = nullptr);
    void fillTimeline(QString from_id = "");
    bool canFetchMore(const QModelIndex &parent) const;
};

class ThreadWindow : public QWidget
{
    Q_OBJECT

public:
    ThreadWindow(Michabo::AccountManager *manager, QString thread_id, QWidget *parent = nullptr);

private:
    Michabo::AccountManager *m_manager;
    QString m_id;

    ThreadModel *m_model;
    TimelineWidget *m_timeline_widget;
    QVBoxLayout *m_vbox_layout;
};

#endif // THREADWINDOW_H
