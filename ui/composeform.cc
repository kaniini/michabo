#include "composeform.h"
#include <QTextCursor>
#include <QFileDialog>

ComposeForm::ComposeForm(Michabo::AccountManager *manager, QWidget *parent)
    : QWidget(parent),
      m_account(manager->selectedAccount()),
      m_subject(new QLineEdit(this)),
      m_content(new QTextEdit(this)),
      m_button_box(new QDialogButtonBox(this)),
      m_send_button(new QPushButton(this)),
      m_cancel_button(new QPushButton(this)),
      m_post_length_counter(new QLabel(this)),
      m_bottom_box(new QHBoxLayout()),
      m_layout(new QVBoxLayout(this)),
      m_content_type(new QComboBox),
      m_attach_button(new QPushButton(this)),
      m_reply_to(nullptr),
      m_scratch(std::make_shared<Michabo::Post>(m_account))
{
    m_attachment_editor = new AttachmentEditorWidget(m_scratch);

    resize(450, 250);

    setWindowTitle(tr("Compose Status"));

    QObject::connect(m_send_button, &QPushButton::clicked, [=] () {
        m_account->postStatus(m_scratch);
        this->deleteLater();
    });

    QObject::connect(m_cancel_button, &QPushButton::clicked, [=] () {
        this->deleteLater();
    });

    m_subject->setPlaceholderText(tr("Subject (optional)"));
    m_layout->addWidget(m_subject);

    m_content->setPlaceholderText(tr("What is on your mind?"));
    m_content->setFocus ();
    m_layout->addWidget(m_content);

    m_send_button->setIcon(QIcon::fromTheme("document-new"));
    m_send_button->setText(tr("Send"));

    m_cancel_button->setIcon(QIcon::fromTheme("window-close"));
    m_cancel_button->setText(tr("Cancel"));

    m_button_box->addButton(m_send_button, QDialogButtonBox::AcceptRole);
    m_button_box->addButton(m_cancel_button, QDialogButtonBox::RejectRole);

    // My apologies to those who prefer bitsets or a number of booleans to represent
    // this logic.
    using ContentType = Michabo::Account::AllowedContentType;
    auto allowed_content_types = m_account->allowedContentTypes();
    if (allowed_content_types & ContentType::PlainText)
        m_content_type->addItem (tr("Plain text"), "text/plain");
    if (allowed_content_types & ContentType::BBCode)
        m_content_type->addItem (tr("BBCode"), "text/bbcode");
    if (allowed_content_types & ContentType::Html)
        m_content_type->addItem (tr("HTML"), "text/html");
    if (allowed_content_types & ContentType::Markdown)
        m_content_type->addItem (tr("Markdown"), "text/markdown");
    m_content_type->setCurrentIndex (0);

    m_post_length_counter->setText(QString::number(m_account->max_post_length()));
    QObject::connect(m_subject, &QLineEdit::textChanged, this, &ComposeForm::updateScratch);
    QObject::connect(m_content, &QTextEdit::textChanged, this, &ComposeForm::updateScratch);
    QObject::connect(m_content_type, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &ComposeForm::updateContentType);

    m_attach_button->setIcon (QIcon::fromTheme ("mail-attachment"));
    m_attach_button->setText (tr("Attach File"));

    QObject::connect(m_attach_button, &QPushButton::clicked, this, &ComposeForm::uploadAttachment);

    m_bottom_box->addWidget (m_attach_button);

    m_bottom_box->addWidget(m_post_length_counter, 1, Qt::AlignRight);

    if (m_content_type->count() > 1)
        m_bottom_box->addWidget(m_content_type);

    m_layout->addLayout(m_bottom_box);
    m_layout->addWidget(m_button_box);

    m_layout->addWidget(m_attachment_editor);

    QObject::connect(m_account, &Michabo::Account::attachmentUploaded, [=] (std::shared_ptr<Michabo::Post> p, std::shared_ptr<Michabo::Attachment>) {
        if (p.get () != m_scratch.get ())
            return;

        m_attachment_editor->updateDirty ();
    });

    QObject::connect(m_account, &Michabo::Account::invalidatedPost, [=] (Michabo::Post *p) {
        if (p != m_scratch.get ())
            return;

        m_attachment_editor->updateDirty ();
    });
}

void ComposeForm::updateScratch()
{
    auto subject_len = m_subject->text().length();
    auto content_len = m_content->toPlainText().length();
    ssize_t remain_len = m_account->max_post_length() - (subject_len + content_len);

    m_post_length_counter->setText(QString::number(remain_len));

    m_send_button->setEnabled(remain_len >= 0);

    m_scratch->m_content = m_content->toPlainText();
    m_scratch->m_subject = m_subject->text();
    m_scratch->m_content_type = m_content_type->currentData().toString();
}

void ComposeForm::updateContentType()
{
    m_scratch->m_content_type = m_content_type->currentData().toString();
}

void ComposeForm::setReplyTo(std::shared_ptr<Michabo::Post> p)
{
    m_reply_to = p;

    setWindowTitle (tr ("Reply"));

    QString replyTemplate;
    for (auto m : p->m_mentions)
    {
        replyTemplate += QString ("@%1 ").arg (m);
    }

    m_content->setText (replyTemplate);
    m_content->moveCursor (QTextCursor::End);

    if (! m_reply_to->m_subject.isEmpty ())
    {
        QString subjectTemplate;

        if (! m_reply_to->m_subject.startsWith ("re:"))
            subjectTemplate = QString ("re: %1").arg (m_reply_to->m_subject);
        else
            subjectTemplate = m_reply_to->m_subject;

        m_subject->setText (subjectTemplate);
    }

    m_scratch->m_reply_target_id = m_reply_to->m_reply_target_id;
    m_scratch->m_visibility = m_reply_to->m_visibility;
}

void ComposeForm::uploadAttachment()
{
    QString filename = QFileDialog::getOpenFileName (this, tr("Attach File"));
    QFile *file = new QFile (filename);
    QFileInfo info (filename);

    file->open(QFile::ReadOnly);
    m_account->upload (m_scratch, file, info.fileName ());
}
