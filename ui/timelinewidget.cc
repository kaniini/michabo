#include <QtGlobal>
#include <QAbstractScrollArea>
#include <QDesktopServices>
#include <QHeaderView>
#include <QBitmap>
#include <QPalette>
#include <QPixmapCache>

#include "timelinewidget.h"
#include "composeform.h"
#include "threadwindow.h"
#include "iconutils.h"

// control the size of avatars
#define AVATAR_ICON_SIZE            48
#define AVATAR_ICON_MARGIN          4

// control the size of the actionbar
#define ACTIONBAR_CONTAINER_MARGIN  8
#define ACTIONBAR_CONTAINER_HEIGHT  (ACTIONBAR_ICON_SIZE + (ACTIONBAR_CONTAINER_MARGIN * 2))
#define ACTIONBAR_ICON_SPACING      20

// control wordwrapping edge
#define WORDWRAP_EDGE               32

// control attachment grid spacing
#define ATTACHMENT_GRID_HSPACING    4
#define ATTACHMENT_GRID_VSPACING    4

static void init_text_document (QTextDocument & doc, const QStyleOptionViewItem & option)
{
    QTextOption textOption (doc.defaultTextOption());
    textOption.setWrapMode (QTextOption::WordWrap);
    doc.setDefaultTextOption (textOption);
    doc.setDefaultStyleSheet("a { color: palette(highlight); }");
    doc.setTextWidth(option.widget->rect().width() - (AVATAR_ICON_SIZE + WORDWRAP_EDGE));

    doc.setHtml (option.text);
    doc.setDefaultFont (option.font);
}

void AvatarDelegate::paint(QPainter* painter, const QStyleOptionViewItem & option_, const QModelIndex &index) const
{
    QStyleOptionViewItem option = option_;
    initStyleOption (& option, index);

    QPixmap avatar = index.data(Qt::DecorationRole).value<QPixmap>();
    if (avatar.isNull ())
        return;

    auto alignment = Qt::AlignHCenter | Qt::AlignTop;
    option.rect.moveBottom(option.rect.bottom() + AVATAR_ICON_MARGIN);
    QRect aligned_rect = QStyle::alignedRect(option.direction, alignment, QSize(AVATAR_ICON_SIZE, AVATAR_ICON_SIZE), option.rect);

    painter->save();
    painter->setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform, true);
    painter->drawPixmap(aligned_rect, avatar, avatar.rect());
    painter->restore();
}

QSize AvatarDelegate::sizeHint(const QStyleOptionViewItem &, const QModelIndex &) const
{
    return QSize(AVATAR_ICON_SIZE, AVATAR_ICON_SIZE);
}

// the geometry_cb is used to set up geometry and clipping
// the rect_cb is to used to actually draw or check hitboxes
QSize HTMLDelegate::doGridLayout(const QStyleOptionViewItem &option,
                                 const QModelIndex &index,
                                 std::function<void (const QPoint &, const QSize &)> geometry_cb,
                                 std::function<void (const QRect &, const QImage &, std::shared_ptr<Michabo::Attachment> att)> rect_cb,
                                 std::function<void ()> finalize_cb) const
{
    auto widget = static_cast<const QTreeView *>(option.widget);
    auto model = static_cast<const TimelineModel *>(widget->model());
    auto post = model->internalData(index);
    auto width = option.widget->rect().width();
    auto exclude_space = (AVATAR_ICON_SIZE) + (AVATAR_ICON_MARGIN * 2) + WORDWRAP_EDGE;
    auto usable_width = width - exclude_space;

    // calculate the layout
    bool horizontal_grid = usable_width > 400;
    auto items_per_section = post->m_attachments.size() / 2;

    // if m_attachments size is 2, then force horizontal layout
    if (post->m_attachments.size() <= 2)
    {
        horizontal_grid = true;
        items_per_section = 2;
    }
    // if m_attachments size < 4, and width allows horizontal grid, do a single row layout
    else if (post->m_attachments.size() <= 4 && horizontal_grid)
    {
        items_per_section = post->m_attachments.size();
    }

    QList<std::shared_ptr<Michabo::Attachment>> processed_attachments;
    qreal attachment_height = 0.;

    // usable item width: usable_width / items_per_section
    auto usable_item_width = qMin(usable_width / items_per_section, 300);

    // lay out the grid, taking the tallest item and placing it in section_heights
    qreal tallest_in_section = 0.;
    for (auto att : post->m_attachments)
    {
        if (att->m_preview.isNull ())
            continue;

        auto final_item_width = usable_item_width;
        if (horizontal_grid)
            final_item_width -= ATTACHMENT_GRID_HSPACING;

        if (! att->m_scratch.isNull())
        {
            // we cache the scratch image, but it may be stale if the layout really did change
            // so rescale it if necessary
            if (att->m_scratch.width() != final_item_width)
                att->m_scratch = att->m_preview.scaledToWidth(final_item_width);
        }
        else
            att->m_scratch = att->m_preview.scaledToWidth(final_item_width);

        QImage & scratch = att->m_scratch;
        qreal height = scratch.height() + ATTACHMENT_GRID_VSPACING;

        tallest_in_section = qMax(tallest_in_section, height);

        processed_attachments.push_back(att);

        if (processed_attachments.size() % items_per_section == 0)
        {
            attachment_height += tallest_in_section;
            tallest_in_section = 0;
        }
    }

    QSize geometry = QSize (usable_width, attachment_height);

    // we now have our geometry
    if (geometry_cb)
        geometry_cb(QPoint (0, attachment_height), QSize (usable_width, attachment_height));

    // now composite the layout -- all rects are calculated from 0, 0
    if (rect_cb)
    {
        QPoint drawPoint = QPoint(ATTACHMENT_GRID_HSPACING, 0);

        tallest_in_section = 0.;
        processed_attachments.clear();
        for (auto att : post->m_attachments)
        {
            QImage &scratch = att->m_scratch;

            if (scratch.isNull ())
                continue;

            QRect attachmentRect = QRect(drawPoint, scratch.size());
            rect_cb(attachmentRect, att->m_preview, att);

            qreal height = scratch.height() + ATTACHMENT_GRID_VSPACING;
            tallest_in_section = qMax(tallest_in_section, height);

            drawPoint += QPoint(scratch.width() + ATTACHMENT_GRID_HSPACING, 0);

            processed_attachments.push_back(att);

            if (processed_attachments.size() % items_per_section == 0)
            {
                auto y = drawPoint.y() + tallest_in_section;
                drawPoint = QPoint(ATTACHMENT_GRID_HSPACING, y);
            }
        }
    }

    if (finalize_cb)
        finalize_cb();

    // all done, return the geometry
    return geometry;
}

void HTMLDelegate::paint(QPainter* painter, const QStyleOptionViewItem & option_, const QModelIndex &index) const
{
    QStyleOptionViewItem option = option_;
    initStyleOption (& option, index);

    auto cursor = option.widget->mapFromGlobal (QCursor::pos ());

    // paint the document
    QTextDocument doc;
    QStyle * style = option.widget->style();
    init_text_document (doc, option);

    // cache the document if we're hovering over it
    auto parentView = qobject_cast<QAbstractItemView *>(parent());
    if ((option.state & QStyle::State_MouseOver)
            && (option.rect.contains(parentView->viewport()->mapFromGlobal(QCursor::pos())))
            && (option.state & QStyle::State_Enabled))
    {
        // work around Qt hating people who keep state
        const_cast<HTMLDelegate *>(this)->setDocument(option);
    }

    // Painting item without text
    option.text = QString ();
    style->drawControl (QStyle::CE_ItemViewItem, & option, painter);

    QAbstractTextDocumentLayout::PaintContext ctx;

    // Color group logic imitating qcommonstyle.cpp
    QPalette::ColorGroup cg =
        ! (option.state & QStyle::State_Enabled) ? QPalette::Disabled :
        ! (option.state & QStyle::State_Active) ? QPalette::Inactive : QPalette::Normal;

    // Highlighting text if item is selected
    if (option.state & QStyle::State_Selected)
        ctx.palette.setColor (QPalette::Text, option.palette.color (cg, QPalette::HighlightedText));
    else
        ctx.palette.setColor (QPalette::Text, option.palette.color (cg, QPalette::Text));

    QRect textRect = style->subElementRect (QStyle::SE_ItemViewItemText, & option);
    painter->save ();
    painter->translate (textRect.topLeft ());
    painter->setClipRect (textRect.translated (-textRect.topLeft ()));
    doc.documentLayout ()->draw (painter, ctx);
    painter->restore ();

    // Action bar
    QStyleOptionViewItem action_option;
    initStyleOption (& action_option, index);

    QPoint abTop = textRect.bottomLeft () - QPoint (0, ACTIONBAR_CONTAINER_HEIGHT);
    QRect abRect = QRect (abTop, QSize (textRect.width (), ACTIONBAR_CONTAINER_HEIGHT));
    QPoint marginPoint = QPoint(ACTIONBAR_CONTAINER_MARGIN, ACTIONBAR_CONTAINER_MARGIN);
    QPoint spacingPoint = QPoint(ACTIONBAR_ICON_SPACING, 0);
    QSize iconSize = QSize(ACTIONBAR_ICON_SIZE, ACTIONBAR_ICON_SIZE);

    auto p = static_cast<const TimelineModel*>(index.model())->internalData(index);
    bool favorited = p->m_is_favorited,
         repeated = p->m_is_repeated,
         attachments_visible = p->m_attachments_visible;

    // translate cursor position to actionbar position
    cursor -= abRect.topLeft();

    painter->save ();

    painter->translate (abRect.topLeft());
    painter->setClipRect (abRect.translated (-abRect.topLeft ()));

#ifdef SHOW_ACTION_BAR_HITBOXES
    painter->fillRect (abRect.translated (-abRect.topLeft ()), Qt::red);
#endif

    QRect replyIconRect = QRect (marginPoint, iconSize);
    painter->combinedTransform();
#ifdef SHOW_ACTION_BAR_HITBOXES
    painter->fillRect (replyIconRect, Qt::green);
#endif
    painter->drawPixmap(replyIconRect, pixmap_from_theme (option.widget->palette(),
                                                          "michabo-action-reply",
                                                          ":/embedded-assets/icons/action-reply.svg",
                                                          proper_icon_state (replyIconRect, cursor)));

    QRect favIconRect = QRect (replyIconRect.topRight() + spacingPoint, iconSize);
#ifdef SHOW_ACTION_BAR_HITBOXES
    painter->fillRect (favIconRect, Qt::green);
#endif
    painter->drawPixmap(favIconRect, pixmap_from_theme (option.widget->palette(),
                                                        "michabo-action-favorite",
                                                        ":/embedded-assets/icons/action-favorite.svg",
                                                        proper_icon_state(favIconRect, cursor, favorited)));

    QRect repeatIconRect = QRect (favIconRect.topRight() + spacingPoint, iconSize);

#ifdef SHOW_ACTION_BAR_HITBOXES
    painter->fillRect (repeatIconRect, Qt::green);
#endif
    painter->drawPixmap(repeatIconRect, pixmap_from_theme (option.widget->palette(),
                                                           "michabo-action-repeat",
                                                           ":/embedded-assets/icons/action-repeat.svg",
                                                           proper_icon_state (repeatIconRect, cursor, repeated)));

    QRect visIconRect = QRect (repeatIconRect.topRight() + spacingPoint, iconSize);
#ifdef SHOW_ACTION_BAR_HITBOXES
    painter->fillRect (visIconRect, Qt::green);
#endif
    painter->drawPixmap(visIconRect, pixmap_from_theme (option.widget->palette(),
                                                        "michabo-action-visibility",
                                                        ":/embedded-assets/icons/action-visibility.svg",
                                                        proper_icon_state (visIconRect, cursor, attachments_visible)));

    QRect menuIconRect = QRect (visIconRect.topRight() + spacingPoint, iconSize);
#ifdef SHOW_ACTION_BAR_HITBOXES
    painter->fillRect (menuIconRect, Qt::green);
#endif

    painter->drawPixmap(menuIconRect, pixmap_from_theme (option.widget->palette(),
                                                         "michabo-action-menu",
                                                         ":/embedded-assets/icons/ellipsis-h.svg",
                                                         proper_icon_state (menuIconRect, cursor)));

    painter->restore ();

    // Attachments
    // get model from widget, from style option.  get post from model.
    auto widget = static_cast<const QTreeView *>(option.widget);
    auto model = static_cast<const TimelineModel *>(widget->model());
    auto post = model->internalData(index);

    // single attachment layout.
    auto strategy = determineLayoutStrategy(option, index);
    if (strategy == SimpleLayout)
    {
        auto attachment = post->m_attachments.first();
        QImage & scratch = attachment->m_scratch;

        // go backwards from the actionbar top
        QRect attachmentRect = QRect (abRect.topLeft() - QPoint(-8, scratch.height() - 4), scratch.size());

        painter->save ();
        painter->setRenderHints (QPainter::Antialiasing | QPainter::SmoothPixmapTransform, true);

        if (p->m_attachments_visible)
            painter->drawImage (attachmentRect, attachment->m_preview);
        else
        {
            auto col = widget->palette ().text ();
            painter->fillRect (attachmentRect, col);
        }

        painter->restore ();
    }
    else if (strategy == GridLayout)
    {
        painter->save ();
        painter->setRenderHints (QPainter::Antialiasing | QPainter::SmoothPixmapTransform, true);

        auto geometry_cb = [painter, abRect] (const QPoint & point, const QSize & size) {
            auto gridRect = QRect (abRect.topLeft () - point, size);
            painter->translate (gridRect.topLeft ());
        };

        auto paint_cb = [p, widget, painter] (const QRect & rect, const QImage & image, const std::shared_ptr<Michabo::Attachment>) {
            if (p->m_attachments_visible)
                painter->drawImage (rect, image);
            else
            {
                auto col = widget->palette ().text ();
                painter->fillRect (rect, col);
            }
        };

        auto finalize_cb = [painter] () {
            painter->restore ();
        };

        // paint the layout
        doGridLayout (option, index, geometry_cb, paint_cb, finalize_cb);
    }
}

void HTMLDelegate::setDocument(const QStyleOptionViewItem & option)
{
    init_text_document(m_doc, option);
}

HTMLDelegate::LayoutStrategy HTMLDelegate::determineLayoutStrategy(const QStyleOptionViewItem & option, const QModelIndex &index) const
{
    auto widget = static_cast<const QTreeView *>(option.widget);
    auto model = static_cast<const TimelineModel *>(widget->model());
    auto post = model->internalData(index);

    if (! post->m_attachments.size())
        return NoLayout;

    return post->m_attachments.size() == 1 ? SimpleLayout : GridLayout;
}

QSize HTMLDelegate::sizeHint(const QStyleOptionViewItem & option_, const QModelIndex & index) const
{
    QStyleOptionViewItem option = option_;
    initStyleOption (& option, index);

    QTextDocument doc;
    init_text_document (doc, option);

    qreal avatar_height = AVATAR_ICON_SIZE + (AVATAR_ICON_MARGIN * 2);
    qreal doc_height = qMax(avatar_height, doc.size().height());
    qreal actionbar_height = ACTIONBAR_CONTAINER_HEIGHT;

    // get model from widget, from style option.  get post from model.
    auto widget = static_cast<const QTreeView *>(option.widget);
    auto model = static_cast<const TimelineModel *>(widget->model());
    auto post = model->internalData(index);
    auto width = option.widget->rect().width();

    qreal attachment_height = 0.;

    // determine layout strategy and usable space
    auto strategy = determineLayoutStrategy(option, index);
    auto exclude_space = (AVATAR_ICON_SIZE) + (AVATAR_ICON_MARGIN * 2) + WORDWRAP_EDGE;
    auto usable_width = width - exclude_space;
    auto maximum_attachment_width = 300;

    // simple layout: one attachment only
    if (strategy == SimpleLayout)
    {
        auto attachment = post->m_attachments.first();

        // cap usable width to 300px for simple layout
        usable_width = qMin(usable_width, maximum_attachment_width);

        // avatar column = 72px, 32px for horizontal margin
        // 8px for vertical margin
        if (! attachment->m_preview.isNull ())
        {
            attachment->m_scratch = attachment->m_preview.scaledToWidth(usable_width);
            attachment_height = attachment->m_scratch.height() + 8;
        }
    }
    // grid layout: >= 2 attachments
    else if (strategy == GridLayout)
    {
        auto geometry_cb = nullptr;
        auto paint_cb = nullptr;
        auto finalize_cb = nullptr;

        auto size = doGridLayout(option, index, geometry_cb, paint_cb, finalize_cb);
        attachment_height = size.height();
    }

    qreal height = doc_height + actionbar_height + attachment_height;
    //qDebug() << "attachment_height" << attachment_height << "total" << height;

    return QSize (width, height);
}

bool HTMLDelegate::editorEvent(QEvent *event, QAbstractItemModel *model, const QStyleOptionViewItem &option, const QModelIndex &index)
{
    QMouseEvent *mouse_event = static_cast<QMouseEvent *> (event);
    auto parentView = qobject_cast<QTreeView *> (parent ());
    auto rect = parentView->visualRect (index);
    auto pos = mouse_event->pos () - QPoint (rect.x (), rect.y ());

    // get our model
    auto timeline_model = static_cast<TimelineModel *>(model);

    if (event->type() == QEvent::MouseButtonDblClick)
    {
        timeline_model->showThreadWindow(index);
        return true;
    }

    if (event->type() != QEvent::MouseButtonRelease && event->type() != QEvent::MouseMove)
        return false;

    // check if we are in a hitbox for a link
    auto anchor = m_doc.documentLayout ()->anchorAt (pos);

    if (anchor.isEmpty ())
    {
        // we are not in a hitbox for a link
        parentView->unsetCursor ();

        // translate our click point back to reality
        // the HTML part needed a perspective from 0, 0
        pos = mouse_event->pos ();

        // check and see if we are in a hitbox for the action bar
        QPoint abTop = rect.bottomLeft () - QPoint (0, ACTIONBAR_CONTAINER_HEIGHT);
        QRect abRect = QRect (abTop, QSize (rect.width (), ACTIONBAR_CONTAINER_HEIGHT));
        QPoint spacingPoint = QPoint(ACTIONBAR_ICON_SPACING, 0);
        QPoint marginPoint = QPoint(ACTIONBAR_CONTAINER_MARGIN, ACTIONBAR_CONTAINER_MARGIN);
        QSize iconSize = QSize(ACTIONBAR_ICON_SIZE, ACTIONBAR_ICON_SIZE);

        if (event->type() == QEvent::MouseMove && abRect.contains(pos))
        {
            timeline_model->handleMouseOver (index);
            return true;
        }

        // if this is not a click, then we're done processing
        if (event->type () != QEvent::MouseButtonRelease)
            return false;

        // we appear to be inside the action bar hitbox, lets see if we are inside any
        // of the actual action hitboxes
        QRect replyIconRect = QRect (abRect.topLeft () + marginPoint, iconSize);
        QRect favIconRect = QRect (replyIconRect.topRight () + spacingPoint, iconSize);
        QRect repeatIconRect = QRect (favIconRect.topRight () + spacingPoint, iconSize);
        QRect visIconRect = QRect (repeatIconRect.topRight () + spacingPoint, iconSize);
        QRect menuIconRect = QRect (visIconRect.topRight () + spacingPoint, iconSize);

        if (replyIconRect.contains (pos))
        {
            timeline_model->actionReply (index);
            return true;
        }

        if (favIconRect.contains (pos))
        {
            timeline_model->actionFavorite (index);
            return true;
        }

        if (repeatIconRect.contains (pos))
        {
            timeline_model->actionRepeat (index);
            return true;
        }

        if (visIconRect.contains (pos))
        {
            timeline_model->actionVis (index);
            return true;
        }

        if (menuIconRect.contains (pos))
        {
            timeline_model->actionMenu (index);
            return true;
        }

        // are we in a hitbox for an attachment?
        auto widget = static_cast<const QTreeView *>(option.widget);
        auto model = static_cast<const TimelineModel *>(widget->model());
        auto post = model->internalData(index);

        // if we have no visible attachments we can't be in a hitbox for an attachment
        if (post->m_attachments.size() == 0 || ! post->m_attachments_visible)
            return false;

        // are we using the simple layout or the grid layout?
        if (determineLayoutStrategy(option, index) == SimpleLayout)
        {
            auto attachment = post->m_attachments.first();
            QImage & scratch = attachment->m_scratch;

            // go backwards from the actionbar top
            QRect attachmentRect = QRect (abRect.topLeft () - QPoint(-8, scratch.height () - 4), scratch.size ());

            if (attachmentRect.contains (pos))
            {
                QDesktopServices::openUrl (attachment->m_url);
                return true;
            }
        }
        else
        {
            auto geometry_cb = [&] (const QPoint &, const QSize & size) {
                auto topLeft = abRect.topLeft () - QPoint (0, size.height ());
                pos -= QPoint (rect.x(), topLeft.y());
            };
            auto rect_cb = [&] (const QRect &rect, const QImage &, const std::shared_ptr<Michabo::Attachment> att) {
                if (! rect.contains (pos))
                    return;

                QDesktopServices::openUrl (att->m_url);
            };
            auto finalize_cb = [&] () {
                pos = mouse_event->pos ();
            };

            doGridLayout (option, index, geometry_cb, rect_cb, finalize_cb);
        }
    }
    else
    {
        // we are in a hitbox for a link
        parentView->setCursor (Qt::PointingHandCursor);
        if (event->type () != QEvent::MouseButtonRelease)
            return false;

        if (anchor == "michabo:toggle")
        {
            timeline_model->actionExpand (index);
            return true;
        }

        QDesktopServices::openUrl (anchor);
        return true;
    }

    return false;
}

TimelineModel::TimelineModel(Michabo::AccountManager *manager, QString timeline_name, QObject *parent)
    : QAbstractListModel(parent),
      m_timeline_name(timeline_name),
      m_manager(manager),
      m_account(m_manager->selectedAccount())
{
    if (timeline_name != "home" && timeline_name != "public" && timeline_name != "federated")
        return;

    QObject::connect(m_manager, &Michabo::AccountManager::accountSelected, [=] (Michabo::Account *account) {
        m_account = account;

        beginResetModel();
        m_timeline.clear();
        endResetModel();

        fillTimeline();
    });
    QObject::connect(m_manager, &Michabo::AccountManager::fetchedTimeline, this, &TimelineModel::fetchedTimeline);
    QObject::connect(m_manager, &Michabo::AccountManager::invalidated, [=] (Michabo::Account *account) {
        if (m_account == account) {
            qDebug() << "Invalidating account" << account;

            beginResetModel();
            m_timeline.clear();
            endResetModel();

            fillTimeline();
        }
    });

    fillTimeline();
}

void TimelineModel::fillTimeline(QString from_id)
{
    (void) from_id;

    m_fetching = true;

    if (m_account)
        m_account->fetchTimeline(m_timeline_name, from_id);
}

void TimelineModel::fetchMore(const QModelIndex &parent)
{
    (void) parent;

    if (m_timeline.size() < 1)
        return;

    auto p = m_timeline.last();

    fillTimeline(p->m_post_id);
}

bool TimelineModel::canFetchMore(const QModelIndex &parent) const
{
    (void) parent;

    if (m_fetching)
        return false;

    if (time(nullptr) <= m_last_fetch)
        return false;

    return true;
}

void TimelineModel::fetchedTimeline(Michabo::Account *account, QString original_name, QList<std::shared_ptr<Michabo::Post>> posts)
{
    m_fetching = false;

    // make sure the timeline update is for us
    if (account != m_account || original_name != m_timeline_name)
        return;

    if (posts.isEmpty())
        return;

    int row, last;

    if (! m_timeline.isEmpty())
    {
        auto post_old = m_timeline.first();
        auto post_new = posts.first();

        if (post_old->m_post_id > post_new->m_post_id)
        {
            row = m_timeline.size();
            last = row + posts.size() - 1;
            m_timeline += posts;
        }
        else
        {
            row = 0;
            last = posts.size();
            m_timeline = posts + m_timeline;
        }
    }
    else
    {
        row = 0;
        last = posts.size();
        m_timeline = posts;
    }

    for (auto p : m_timeline)
        p->fetchAttachmentPreviews();

    beginInsertRows(QModelIndex(), row, last);
    endInsertRows();

    m_last_fetch = time(nullptr);
}

int TimelineModel::rowCount(const QModelIndex &parent) const
{
    (void) parent;

    return m_timeline.size();
}

int TimelineModel::columnCount(const QModelIndex &parent) const
{
    (void) parent;

    return NColumns;
}

// this is even more extremely cursed
std::shared_ptr<Michabo::Post> TimelineModel::internalData(const QModelIndex &index) const
{
    int row = index.row();
    return m_timeline[row];
}

QVariant TimelineModel::data(const QModelIndex &index, int role) const
{
    int row = index.row();
    int col = index.column();
    auto p = m_timeline[row];
    if (role == Qt::DecorationRole && ! p->m_author_identity->m_avatar.isNull() && col == TimelineModel::Avatar)
        return QPixmap::fromImage(p->m_author_identity->m_avatar);

    if (role != Qt::DisplayRole)
        return QVariant();
    if(role >= Qt::UserRole){
        if(role == Qt::UserRole+1)
            return QVariant(p->m_is_repeated);
        else
            return QVariant(p->m_is_favorited);
    }
    switch(col)
    {
    case Data:
        auto repeat = QString();
        auto author = QString("<div><strong>%1</strong></div><div><small>%2</small></div>").arg(
                    p->m_author_identity->m_display_name, p->m_author_identity->m_acct);
        auto content = p->m_is_expanded ? QString("<p>%1</p>").arg(p->m_content) : QString();
        auto subject = QString();
        auto show_more = ! p->m_is_expanded ? tr("Show More") : tr("Show Less");

        if (! p->m_subject.isEmpty ())
            subject = QString("<div>%1 [<a href=\"michabo:toggle\">%2</a>]").arg(p->m_subject, show_more);

        if (p->m_repeat)
            repeat = QString("<div><small><strong>%1</strong> %2</small></div>").arg(
                                  p->m_repeat_identity->m_display_name, tr("repeated"));

        return repeat + author + subject + content;
    }

    return QVariant();
}

void TimelineModel::actionReply(const QModelIndex &index)
{
    int row = index.row ();
    auto p = m_timeline[row];

    emit wantReply (m_account, p, index);
}

void TimelineModel::actionMenu(const QModelIndex &index)
{
    int row = index.row ();
    auto p = m_timeline[row];

    emit wantMenu (m_account, p, index);
}

void TimelineModel::actionFavorite(const QModelIndex &index)
{
    int row = index.row ();
    auto p = m_timeline[row];

    if (! p->m_is_favorited)
    {
        m_account->favorite (p);
        p->m_is_favorited = true;
    }
    else
    {
        m_account->unfavorite (p);
        p->m_is_favorited = false;
    }

    emit dataChanged(index,index);
}

void TimelineModel::actionRepeat(const QModelIndex &index)
{
    int row = index.row();
    auto p = m_timeline[row];

    if (! p->m_is_repeated)
    {
        m_account->repeat (p);
        p->m_is_repeated = true;
    }
    else
    {
        m_account->unrepeat (p);
        p->m_is_repeated = false;
    }

    emit dataChanged(index, index);
}

void TimelineModel::actionVis(const QModelIndex &index)
{
    int row = index.row ();
    auto p = m_timeline[row];

    p->m_attachments_visible ^= true;

    emit dataChanged(index, index);
}

void TimelineModel::actionExpand(const QModelIndex &index)
{
    int row = index.row ();
    auto p = m_timeline[row];

    p->m_is_expanded ^= true;

    emit dataChanged(index, index);
}

void TimelineModel::showThreadWindow(const QModelIndex &index)
{
    int row = index.row ();
    auto p = m_timeline[row];

    auto win = new ThreadWindow(m_manager, p->m_post_id);
    win->show();
}

void TimelineModel::handleMouseOver(const QModelIndex &index)
{
    emit dataChanged(index, index);
}

TimelineWidget::TimelineWidget(Michabo::AccountManager *manager, QString, TimelineModel *model, QWidget *parent)
    : QTreeView(parent),
      m_model(model),
      m_html_delegate(new HTMLDelegate),
      m_avatar_delegate(new AvatarDelegate)
{
    setModel(m_model);
    setRootIsDecorated(false);
    setHeaderHidden(true);
    setAlternatingRowColors(true);
    setWordWrap(true);
    setMouseTracking(true);
    setItemDelegateForColumn(TimelineModel::Avatar, m_avatar_delegate);
    setItemDelegateForColumn(TimelineModel::Data, m_html_delegate);
    setSelectionMode(QAbstractItemView::NoSelection);
    setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);
    header()->resizeSection(0, AVATAR_ICON_SIZE + (AVATAR_ICON_MARGIN * 2));

    m_avatar_delegate->setParent(this);
    m_html_delegate->setParent(this);

    QObject::connect(m_model, &TimelineModel::wantReply, [=] (const Michabo::Account *, std::shared_ptr<Michabo::Post> p, const QModelIndex &) {
        qDebug() << "Reply to post" << p->m_reply_target_id;

        auto composer = new ComposeForm (manager);
        composer->setReplyTo (p);
        composer->show ();
    });

    QObject::connect(manager, &Michabo::AccountManager::invalidatedPost, [=] () {
        m_model->disallowUpdates();
        m_model->layoutChanged();
    });
}

// this is *extremely* cursed but whatever
void TimelineWidget::resizeEvent(QResizeEvent *event)
{
    (void) event;

    m_model->layoutChanged();
}
