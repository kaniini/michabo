#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "core/accountmanager.h"
#include "identitywidget.h"
#include "timelineselectorwidget.h"
#include "preferenceswindow.h"
#include "aboutwindow.h"

#include <QMainWindow>
#include <QWidget>
#include <QVBoxLayout>
#include <QPushButton>
#include <QLabel>
#include <QToolBar>
#include <QToolButton>

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr, Michabo::AccountManager *manager = nullptr);
    ~MainWindow();

private:
    QWidget *m_main_container;
    QVBoxLayout *m_main_layout;
    //IdentityWidget *m_identity_widget;
    QToolBar *m_toolbar;
    QToolButton *m_compose_button;
    QToolButton *m_refresh_button;
    QToolButton *m_prefs_button;
    QToolButton *m_about_button;
    PreferencesWindow *m_prefs_window;
    AboutWindow *m_about_window;
    Michabo::AccountManager *m_account_manager;

    void setInstanceName(Michabo::Account *account);
};

#endif // MAINWINDOW_H
