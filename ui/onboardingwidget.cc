#include "onboardingwidget.h"

OnboardingWidget::OnboardingWidget(Michabo::AccountManager *manager, QWidget *parent) :
    QWidget(parent),
    m_layout(new QVBoxLayout(this)),
    m_michabo_icon(new QLabel(this)),
    m_welcome_label(new QLabel(this)),
    m_addaccount_label(new QLabel(this)),
    m_addaccount_button(new AddAccountButton(manager, this)),
    m_account_manager(manager)
{
    QPixmap icon = QIcon(":/embedded-assets/icons/michabo-256.png").pixmap(96, 96);

    m_michabo_icon->setPixmap(icon);
    m_michabo_icon->setAlignment(Qt::AlignCenter);

    m_welcome_label->setAlignment(Qt::AlignCenter);
    m_welcome_label->setText(tr("<big>Welcome to Michabo!</big>"));
    m_addaccount_label->setText(tr("To begin, add a new account by clicking the button below."));

    m_addaccount_button->setText(tr("Add a new account"));

    m_layout->addWidget(m_michabo_icon, 0);
    m_layout->addWidget(m_welcome_label, 0);
    m_layout->addWidget(m_addaccount_label, 0);
    m_layout->addWidget(m_addaccount_button, 0);
}

OnboardingWidget::~OnboardingWidget() {}
