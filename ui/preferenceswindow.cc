#include "preferenceswindow.h"

PreferencesWindow::PreferencesWindow(QWidget * parent)
    : QDialog (parent),
      m_layout (new QVBoxLayout (this)),
      m_prefs_label (new QLabel),
      m_tl_firehose (new QCheckBox),
      m_buttonbox (new QDialogButtonBox),
      m_close_button (new QPushButton)
{
    setWindowTitle (tr("Michabo Preferences"));

    m_prefs_label->setText (tr ("<big>Preferences</big>"));
    m_layout->addWidget (m_prefs_label);

    m_tl_firehose->setText (tr ("Stream new posts onto the home timeline"));
    m_layout->addWidget (m_tl_firehose);

    m_close_button->setText (tr ("Close"));
    m_close_button->setIcon (QIcon::fromTheme ("window-close"));

    m_buttonbox->addButton (m_close_button, QDialogButtonBox::RejectRole);
    m_layout->addWidget (m_buttonbox);

    QSettings settings;

    settings.beginGroup ("Preferences");

    bool tl_firehose = settings.value ("timeline_firehose", true).toBool ();
    m_tl_firehose->setChecked (tl_firehose);

    settings.endGroup ();

    QObject::connect (m_tl_firehose, &QCheckBox::toggled, [=] (bool checked) {
        QSettings settings;

        settings.beginGroup ("Preferences");
        settings.setValue ("timeline_firehose", checked);
        settings.endGroup ();
    });

    QObject::connect (m_close_button, &QPushButton::clicked, [=] () {
        close ();
    });
}
