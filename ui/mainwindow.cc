#include <QWidget>
#include <QButtonGroup>
#include <QDockWidget>

#include "core/account.h"
#include "mainwindow.h"
#include "composeform.h"
#include "iconutils.h"
#include "preferenceswindow.h"
#include "timelinewidget.h"

class TimelineContainerWidget : public QDockWidget {
public:
    TimelineContainerWidget(QString timeline, Michabo::AccountManager *manager, QWidget * parent = nullptr);

private:
    Michabo::AccountManager *m_manager;
    Michabo::Account *m_account;
    TimelineModel *m_timeline_model;
    TimelineWidget *m_timeline;
};

TimelineContainerWidget::TimelineContainerWidget(QString timeline, Michabo::AccountManager *manager, QWidget *parent) :
    QDockWidget (parent),
    m_manager (manager),
    m_timeline_model (new TimelineModel(m_manager, timeline)),
    m_timeline (new TimelineWidget (m_manager, timeline, m_timeline_model))
{
    setWindowTitle(timeline);
    setWidget(m_timeline);
}

MainWindow::MainWindow(QWidget *parent,
                       Michabo::AccountManager *manager)
    : QMainWindow(parent),
      m_main_container(new QWidget()),
      m_main_layout(new QVBoxLayout(m_main_container)),
      //m_identity_widget(new IdentityWidget(manager, m_main_container)),
      m_toolbar(new QToolBar(m_main_container)),
      m_compose_button(new QToolButton(m_toolbar)),
      m_refresh_button(new QToolButton(m_toolbar)),
      m_prefs_button(new QToolButton(m_toolbar)),
      m_about_button(new QToolButton(m_toolbar)),
      m_prefs_window(new PreferencesWindow()),
      m_about_window(new AboutWindow()),
      m_account_manager(manager)
{
    // main layout
    m_main_layout->setContentsMargins(0, 0, 0, 0);
    //m_main_layout->addWidget(m_identity_widget);

    addToolBar(Qt::BottomToolBarArea, m_toolbar);
    setDockNestingEnabled(true);

    // compose button
    m_compose_button->setIcon(pixmap_from_theme (palette (), "michabo-action-compose",
                                                 ":/embedded-assets/icons/action-compose.svg",
                                                 Default, 24));
    m_compose_button->setText(tr("Compose"));
    m_compose_button->setToolTip(tr("Compose"));
    QObject::connect(m_compose_button, &QToolButton::clicked, [=] () {
        auto form = new ComposeForm(m_account_manager);
        form->show();
    });

    m_toolbar->addWidget(m_compose_button);

    // refresh button
    m_refresh_button->setIcon(pixmap_from_theme (palette (), "michabo-action-refresh",
                                                 ":/embedded-assets/icons/action-refresh.svg",
                                                 Default, 24));
    m_refresh_button->setText(tr("Refresh"));
    m_refresh_button->setToolTip(tr("Refresh"));
    QObject::connect(m_refresh_button, &QToolButton::clicked, [=] () {
         manager->selectedAccount()->invalidate();
    });

    m_toolbar->addWidget(m_refresh_button);

    // prefs button
    m_prefs_button->setIcon (pixmap_from_theme (palette (), "michabo-action-prefs",
                                                ":/embedded-assets/icons/action-preferences.svg",
                                                Default, 24));
    m_prefs_button->setText (tr("Preferences"));
    m_prefs_button->setToolTip (tr("Preferences"));

    QObject::connect(m_prefs_button, &QToolButton::clicked, [=] () {
        m_prefs_window->show();
    });

    QWidget * spacer = new QWidget;
    spacer->setSizePolicy (QSizePolicy::Expanding, QSizePolicy::Expanding);
    m_toolbar->addWidget(spacer);

    QObject::connect(m_about_button, &QToolButton::clicked, [=] () {
        m_about_window->show();
    });

    m_about_button->setIcon (pixmap_from_theme (palette (), "michabo-action-about",
                                                ":/embedded-assets/icons/action-about.svg",
                                                Default, 24));
    m_about_button->setText (tr("About"));
    m_about_button->setToolTip (tr("About"));
    m_toolbar->addWidget(m_about_button);

    m_toolbar->addWidget(m_prefs_button);

    QObject::connect(m_account_manager, &Michabo::AccountManager::fetchedInstanceMetadata, [=] (Michabo::Account *account) {
        if (account != m_account_manager->selectedAccount())
            return;

        setInstanceName(account);
    });
    QObject::connect(m_account_manager, &Michabo::AccountManager::accountSelected, [=] (Michabo::Account *account) {
        setInstanceName(account);
    });

    // for now, generate three timeline containers
    auto home_tl = new TimelineContainerWidget("home", m_account_manager);
    addDockWidget(Qt::LeftDockWidgetArea, home_tl);

    auto local_tl = new TimelineContainerWidget("public", m_account_manager);
    addDockWidget(Qt::LeftDockWidgetArea, local_tl);

    auto federated_tl = new TimelineContainerWidget("federated", m_account_manager);
    addDockWidget(Qt::LeftDockWidgetArea, federated_tl);

    // dummy widget to make Qt happy
    setCentralWidget(m_main_container);
    m_main_container->hide();
}

MainWindow::~MainWindow()
{

}

void MainWindow::setInstanceName(Michabo::Account *account)
{
    QString instance_name = account->instance_name();

    if (instance_name.isEmpty())
        setWindowTitle("Michabo");

    setWindowTitle(QString("Michabo (%1)").arg(instance_name));
}
