#include "identitywidget.h"

IdentityWidget::IdentityWidget(Michabo::AccountManager *manager, QWidget *parent)
    : QWidget(parent),
      m_manager(manager),
      m_account(manager->selectedAccount()),
      m_display_name(new QLabel(this)),
      m_acct(new QLabel(this)),
      m_avatar(new AvatarWidget(this)),
      m_hbox_layout(new QHBoxLayout(this)),
      m_vbox_layout(new QVBoxLayout())
{
    QObject::connect(m_manager, &Michabo::AccountManager::accountSelected, this, &IdentityWidget::accountSelected);
    QObject::connect(m_manager, &Michabo::AccountManager::identityChanged, this, &IdentityWidget::accountSelected);

    accountSelected(m_manager->selectedAccount());

    m_vbox_layout->addWidget(m_display_name);
    m_vbox_layout->addWidget(m_acct);

    m_hbox_layout->addWidget(m_avatar);
    m_hbox_layout->addLayout(m_vbox_layout, 1);
}

void IdentityWidget::accountSelected(Michabo::Account *account)
{
    if (! account || account != m_manager->selectedAccount())
        return;

    qDebug() << "Updating identity widget for" << account;

    updateIdentity(account->identity());
}

void IdentityWidget::updateIdentity(const Michabo::Identity &identity)
{
    QString disp = QString("<big>%1</big>").arg(identity.m_display_name);

    m_display_name->setText(disp);
    m_acct->setText(identity.m_acct);

    qDebug() << "Avatar" << identity.m_avatar;

    if (! identity.m_avatar.isNull())
    {
        m_avatar->setAvatar(identity.m_avatar);
    }
}
