#include "timelineselectorwidget.h"
#include "timelinewidget.h"

TimelineSelectorWidget::TimelineSelectorWidget(Michabo::AccountManager *manager, QWidget *parent)
    : QTabWidget(parent),
      m_manager(manager),
      m_account(m_manager->selectedAccount())
{
    setDocumentMode(true);

    m_home_timeline = new TimelineModel(manager, "home");
    m_public_timeline = new TimelineModel(manager, "public");
    m_federated_timeline = new TimelineModel(manager, "federated");

    addTab(new TimelineWidget(m_manager, "home", m_home_timeline, this), "Home");
    addTab(new TimelineWidget(m_manager, "public", m_public_timeline, this), "Public");
    addTab(new TimelineWidget(m_manager, "federated", m_federated_timeline, this), "Federated");
}
