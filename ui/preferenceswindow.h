#ifndef PREFERENCESWINDOW_H
#define PREFERENCESWINDOW_H

#include <QWidget>
#include <QDialog>
#include <QVBoxLayout>
#include <QCheckBox>
#include <QDialogButtonBox>
#include <QLabel>
#include <QPushButton>
#include <QSettings>

class PreferencesWindow : public QDialog
{
public:
    PreferencesWindow(QWidget * parent = nullptr);

private:
    QVBoxLayout *m_layout;
    QLabel *m_prefs_label;
    QCheckBox *m_tl_firehose;
    QDialogButtonBox *m_buttonbox;
    QPushButton *m_close_button;
};

#endif // PREFERENCESWINDOW_H
