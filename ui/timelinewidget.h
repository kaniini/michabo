#ifndef TIMELINEWIDGET_H
#define TIMELINEWIDGET_H

#include <QWidget>
#include <QTreeView>
#include <QStyledItemDelegate>
#include <QPainter>
#include <QTextDocument>
#include <QAbstractTextDocumentLayout>
#include <deque>

#include "core/accountmanager.h"
#include "core/post.h"

class AvatarDelegate : public QStyledItemDelegate
{
protected:
    void paint (QPainter * painter, const QStyleOptionViewItem & option, const QModelIndex & index) const;
    QSize sizeHint (const QStyleOptionViewItem & option, const QModelIndex & index) const;
};

class HTMLDelegate : public QStyledItemDelegate
{
public:
    enum LayoutStrategy {
        NoLayout,
        SimpleLayout,
        GridLayout
    };

protected:
    void paint (QPainter * painter, const QStyleOptionViewItem & option, const QModelIndex & index) const;
    QSize sizeHint (const QStyleOptionViewItem & option, const QModelIndex & index) const;
    bool editorEvent (QEvent * event, QAbstractItemModel * model, const QStyleOptionViewItem & option, const QModelIndex & index);
    void setDocument (const QStyleOptionViewItem & option);

private:
    QTextDocument m_doc;

    LayoutStrategy determineLayoutStrategy(const QStyleOptionViewItem & option_, const QModelIndex & index) const;
    QSize doGridLayout (const QStyleOptionViewItem & option,
                        const QModelIndex & index,
                        std::function<void (const QPoint & point, const QSize & size)> geometry_cb,
                        std::function<void (const QRect & bounded, const QImage & image, const std::shared_ptr<Michabo::Attachment> att)> rect_cb,
                        std::function<void ()> finalize_cb) const;
};

class TimelineModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum {
        Avatar,
        Data,
        NColumns
    };

    TimelineModel(Michabo::AccountManager *manager, QString timeline_name, QObject * parent = nullptr);
    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;

    virtual void fillTimeline(QString from_id = "");

    void actionReply(const QModelIndex &index);
    void actionFavorite(const QModelIndex &index);
    void actionRepeat(const QModelIndex &index);
    void actionMenu(const QModelIndex &index);
    void actionVis(const QModelIndex &index);
    void actionExpand(const QModelIndex &index);
    void handleMouseOver(const QModelIndex &index);
    void showThreadWindow(const QModelIndex &index);

    void disallowUpdates() { m_last_fetch = time(nullptr) + 3; }

    std::shared_ptr<Michabo::Post> internalData(const QModelIndex &index) const;

signals:
    void wantReply(Michabo::Account *account, std::shared_ptr<Michabo::Post> post, const QModelIndex &index);
    void wantMenu(Michabo::Account *account, std::shared_ptr<Michabo::Post> post, const QModelIndex &index);

protected:
    void fetchMore(const QModelIndex &parent);
    bool canFetchMore(const QModelIndex &parent) const;

    QString m_timeline_name;
    Michabo::AccountManager *m_manager;
    Michabo::Account *m_account;

    QList<std::shared_ptr<Michabo::Post>> m_timeline;
    bool m_fetching;
    time_t m_last_fetch;

public slots:
    void fetchedTimeline(Michabo::Account *account, QString original_name, QList<std::shared_ptr<Michabo::Post>> posts);
};

class TimelineWidget : public QTreeView
{
public:
    TimelineWidget(Michabo::AccountManager *manager, QString timeline_name, TimelineModel *model, QWidget *parent = nullptr);

protected:
    void resizeEvent(QResizeEvent *event);

private:
    TimelineModel *m_model;
    HTMLDelegate *m_html_delegate;
    AvatarDelegate *m_avatar_delegate;
};

#endif // TIMELINEWIDGET_H
