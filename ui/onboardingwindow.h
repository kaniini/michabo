#ifndef UI_ONBOARDINGWINDOW_H
#define UI_ONBOARDINGWINDOW_H

#include "mainwindow.h"
#include "onboardingwidget.h"

class OnboardingWindow : public QWidget {
public:
    OnboardingWindow (QWidget * parent = nullptr, Michabo::AccountManager * manager = nullptr, MainWindow * mainwin = nullptr);

private:
    Michabo::AccountManager *m_account_manager;
    MainWindow *m_main_window;
    OnboardingWidget *m_onboarding_widget;
    QVBoxLayout *m_layout;
};

#endif
