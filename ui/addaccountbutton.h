#ifndef ADDACCOUNTBUTTON_H
#define ADDACCOUNTBUTTON_H

#include <QPushButton>

#include "core/accountmanager.h"

class AddAccountButton : public QPushButton
{
public:
    AddAccountButton(Michabo::AccountManager *manager, QWidget *parent = nullptr);

private:
    Michabo::AccountManager *m_manager;
};

#endif // ADDACCOUNTBUTTON_H
