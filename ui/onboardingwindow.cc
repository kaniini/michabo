#include "onboardingwindow.h"

OnboardingWindow::OnboardingWindow (QWidget * parent, Michabo::AccountManager * manager, MainWindow * mainwin) :
    QWidget (parent),
    m_account_manager (manager),
    m_main_window (mainwin),
    m_onboarding_widget (new OnboardingWidget (m_account_manager, this)),
    m_layout (new QVBoxLayout (this))
{
    setWindowTitle("Michabo");

    m_layout->addWidget(m_onboarding_widget);

    QObject::connect(m_account_manager, &Michabo::AccountManager::accountAdded, [=] () {
        hide();
        m_main_window->show();
    });
}
