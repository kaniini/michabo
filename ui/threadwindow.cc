#include "threadwindow.h"

ThreadModel::ThreadModel(Michabo::AccountManager *manager, QString timeline_name, QObject *parent)
    : TimelineModel(manager, timeline_name, parent)
{
    fillTimeline ();
}

void ThreadModel::fillTimeline(QString)
{
    qDebug() << "Fetch thread" << m_timeline_name;

    m_account->fetchThread(m_timeline_name, [=] (QList<std::shared_ptr<Michabo::Post>> posts) {
        qDebug() << "Got" << posts.size () << "posts";

        m_timeline = posts;

        for (auto p : m_timeline)
            p->fetchAttachmentPreviews ();

        beginInsertRows (QModelIndex (), 0, m_timeline.size ());
        endInsertRows ();
    });
}

bool ThreadModel::canFetchMore(const QModelIndex &) const
{
    return false;
}

ThreadWindow::ThreadWindow(Michabo::AccountManager *manager, QString thread_id, QWidget *parent)
    : QWidget(parent),
      m_manager(manager),
      m_id(thread_id)
{
    setWindowTitle (QString (tr("Thread %1").arg (m_id)));
    resize (300, 600);

    m_model = new ThreadModel (m_manager, m_id, this);
    m_timeline_widget = new TimelineWidget (m_manager, m_id, m_model, this);

    m_vbox_layout = new QVBoxLayout (this);
    m_vbox_layout->addWidget (m_timeline_widget);
}
