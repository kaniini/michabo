#ifndef ICONUTILS_H
#define ICONUTILS_H

#include <QIcon>
#include <QImage>
#include <QPixmap>
#include <QPixmapCache>
#include <QBitmap>
#include <QPalette>
#include <QRect>

enum IconState {
    Default,
    Enabled,
    Highlighted
};

#define ACTIONBAR_ICON_SIZE         16
#define ACTIONBAR_ICON_SUPERSIZED   ACTIONBAR_ICON_SIZE * 3

QPixmap pixmap_from_theme (QPalette palette, QString icon_name, QString fallback, IconState state = Default, int size = ACTIONBAR_ICON_SIZE);
IconState proper_icon_state (QRect r, QPoint p, bool enabled = false);

#endif // ICONUTILS_H
