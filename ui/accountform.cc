#include <QDesktopServices>

#include "accountform.h"

AccountForm::AccountForm(Michabo::Account *account, QWidget *parent)
    : QWidget(parent),
      m_account(account),
      m_header(new QLabel(this)),
      m_instructions(new QLabel(this)),
      m_name(new QLineEdit(this)),
      m_instance_uri(new QLineEdit(this)),
      m_auth_button(new QPushButton(this)),
      m_token(new QLineEdit(this)),
      m_buttonbox(new QDialogButtonBox(this)),
      m_save_button(new QPushButton(this)),
      m_cancel_button(new QPushButton(this)),
      m_layout(new QVBoxLayout(this)),
      m_instance_uri_timer(new QTimer(this))
{
    m_instance_uri_timer->setSingleShot(true);

    m_header->setText(tr("<big>Add or Edit Account</big>"));

    m_instructions->setText(tr("Enter a name for your account, then the URL of the instance.\n"
                               "Afterward, click the Authorize button.  A web browser will be\n"
                               "launched allowing you to authorize your account.  Finally,\n"
                               "put the authentication token provided to you into the token\n"
                               "box."));

    m_name->setPlaceholderText(tr("Account name"));

    QObject::connect(m_name, &QLineEdit::textChanged, [=] (const QString &data) {
        m_account->setName(data);
    });

    m_instance_uri->setPlaceholderText(tr("https://example.com"));

    QObject::connect(m_instance_uri, &QLineEdit::textChanged, [=] (const QString &) {
        m_instance_uri_timer->start(500);
    });

    QObject::connect(m_instance_uri_timer, &QTimer::timeout, [=] () {
        m_account->setInstanceUri(m_instance_uri->text());
    });

    m_auth_button->setEnabled(false);
    m_auth_button->setText(tr("Authorize"));

    QObject::connect(m_account, &Michabo::Account::registered, [=] () {
        m_auth_button->setEnabled(true);
    });

    QObject::connect(m_auth_button, &QPushButton::clicked, [=] () {
        QDesktopServices::openUrl(m_account->getAuthorizeUrl());
    });

    m_token->setPlaceholderText(tr("Authorization Token"));
    m_token->setEchoMode(QLineEdit::Password);

    QObject::connect(m_token, &QLineEdit::textChanged, [=] (const QString &data) {
        m_account->setToken(data);
    });

    m_save_button->setText(tr("Save"));
    m_save_button->setIcon(QIcon::fromTheme("document-save"));
    m_save_button->setEnabled(false);

    QObject::connect(m_account, &Michabo::Account::authenticated, [=] () {
        m_save_button->setEnabled(true);
    });

    QObject::connect(m_save_button, &QPushButton::clicked, [=] () {
        emit accountSaved(m_account);
        this->close();
    });

    QObject::connect(m_cancel_button, &QPushButton::clicked, [=] () {
        emit accountRejected(m_account);
        this->close();
    });

    m_cancel_button->setText(tr("Cancel"));
    m_cancel_button->setIcon(QIcon::fromTheme("window-close"));

    m_buttonbox->addButton(m_save_button, QDialogButtonBox::AcceptRole);
    m_buttonbox->addButton(m_cancel_button, QDialogButtonBox::RejectRole);

    m_layout->addWidget(m_header);
    m_layout->addWidget(m_instructions);
    m_layout->addWidget(m_name);
    m_layout->addWidget(m_instance_uri);
    m_layout->addWidget(m_auth_button);
    m_layout->addWidget(m_token);
    m_layout->addWidget(m_buttonbox);

    setLayout(m_layout);
}
