#ifndef NOTIFICATIONIMPL_H
#define NOTIFICATIONIMPL_H

#include <memory>
#include <QObject>

#include "core/account.h"

#if defined(Q_OS_UNIX) && !defined(Q_OS_MAC)
# define USE_DBUS_NOTIFICATIONS
# include <QtDBus>
#endif

class NotificationImpl : public QObject
{
    Q_OBJECT
public:
    explicit NotificationImpl(QObject *parent = nullptr);

signals:

public slots:
    void showNotification(Michabo::Account *a, std::shared_ptr<Michabo::Notification> n);

private:
};

#ifdef USE_DBUS_NOTIFICATIONS

// derived from Snorenotify
struct DBusImageHint
{
    DBusImageHint() = default;
    DBusImageHint(const QImage &img);
    QImage toQImage() const;

    int width;
    int height;
    int rowstride;
    bool hasAlpha;
    int bitsPerSample;
    int channels;
    QByteArray imageData;
    QString _hash;

private:
    static int imageHintID;
};

Q_DECLARE_METATYPE(DBusImageHint)

QDBusArgument &operator<< (QDBusArgument &a, const DBusImageHint &i);
const QDBusArgument &operator>> (const QDBusArgument &a, DBusImageHint &i);

#endif

#endif // NOTIFICATIONXDGIMPL_H
