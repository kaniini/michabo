#include "notificationimpl.h"

#include <QCoreApplication>
#include <QTextDocument>

NotificationImpl::NotificationImpl(QObject *parent) : QObject(parent)
{
#ifdef USE_DBUS_NOTIFICATIONS
    if (! QDBusConnection::sessionBus ().isConnected ())
    {
        qDebug() << "D-Bus session bus not present.";
    }
#endif
}

static QString notification_action (Michabo::Notification::Type type)
{
    switch (type)
    {
    case Michabo::Notification::Favorite:
        return QObject::tr ("favourited your post");

    case Michabo::Notification::Follow:
        return QObject::tr ("followed you");

    case Michabo::Notification::Mention:
        return QObject::tr ("mentioned you");

    case Michabo::Notification::Repeat:
        return QObject::tr ("repeated your post");

    default:
        return "???";
    }
}

static QString notification_message (std::shared_ptr<Michabo::Notification> n)
{
    switch (n->m_type)
    {
    case Michabo::Notification::Favorite:
    case Michabo::Notification::Mention:
    case Michabo::Notification::Repeat:
        if (! n->m_post->m_subject.isEmpty ())
            return n->m_post->m_subject;

        return n->m_post->m_content;

    case Michabo::Notification::Follow:
        return n->m_identity->m_acct;

    default:
        return "???";
    }
}

#if defined(USE_DBUS_NOTIFICATIONS)

int DBusImageHint::imageHintID = qDBusRegisterMetaType<DBusImageHint>();

DBusImageHint::DBusImageHint(const QImage &img)
{
    QImage image (img.convertToFormat (QImage::Format_ARGB32).rgbSwapped ());

    imageData = QByteArray ((char *) image.bits (), image.byteCount ());
    width = image.width ();
    height = image.height ();
    rowstride = image.bytesPerLine ();
    hasAlpha = image.hasAlphaChannel ();
    channels = hasAlpha ? 4 : 3;
    bitsPerSample = image.depth () / channels;
}

QImage DBusImageHint::toQImage() const
{
    return QImage ((uchar *) imageData.data (), width, height, QImage::Format_ARGB32).rgbSwapped ();
}

QDBusArgument &operator<< (QDBusArgument &a, const DBusImageHint &i)
{
    a.beginStructure ();

    a <<
         i.width <<
         i.height <<
         i.rowstride <<
         i.hasAlpha <<
         i.bitsPerSample <<
         i.channels <<
         i.imageData;

    a.endStructure ();

    return a;
}

const QDBusArgument &operator>> (const QDBusArgument &a, DBusImageHint &i)
{
    a.beginStructure ();

    a >>
         i.width >>
         i.height >>
         i.rowstride >>
         i.hasAlpha >>
         i.bitsPerSample >>
         i.channels >>
         i.imageData;

    a.endStructure ();
    return a;
}

void NotificationImpl::showNotification(Michabo::Account *, std::shared_ptr<Michabo::Notification> n)
{
    QVariantMap hints;

    QDBusInterface iface ("org.freedesktop.Notifications",
                          "/org/freedesktop/Notifications",
                          "org.freedesktop.Notifications",
                          QDBusConnection::sessionBus ());

    if (! iface.isValid())
    {
        qDebug() << "D-Bus interface is not valid.";
        return;
    }

    // prepare our message
    QString action = notification_action (n->m_type);
    QString message = notification_message (n);
    QString topic = QString ("%1 %2 (%3)").arg(n->m_identity->m_display_name, action, qApp->applicationName ());

    // prepare the avatar
    DBusImageHint image (n->m_identity->m_avatar);
    hints.insert (QStringLiteral ("image_data"), QVariant::fromValue (image));

    QDBusReply<uint> reply;
    reply = iface.call ("Notify",
                        qApp->applicationName (),
                        (uint) 0,
                        "michabo",
                        topic,
                        message,
                        QStringList (),
                        hints,
                        -1);
}

#elif defined(Q_OS_MAC)

extern void show_notification_osx (QString &topic, QString &message, QImage &avatar);

void NotificationImpl::showNotification(Michabo::Account *, std::shared_ptr<Michabo::Notification> n)
{
    // prepare our message
    QString action = notification_action (n->m_type);
    QString message = notification_message (n);
    QString topic = QString ("%1 %2 (%3)").arg(n->m_identity->m_display_name, action, qApp->applicationName ());

    QTextDocument doc;
    doc.setHtml (message);
    message = doc.toPlainText ();

    show_notification_osx (topic, message, n->m_identity->m_avatar);
}

#else

void NotificationImpl::showNotification(Michabo::Account *, std::shared_ptr<Michabo::Notification>)
{
    qDebug() << "Notification support is unimplemented!";
}

#endif
