#ifndef ONBOARDINGWIDGET_H
#define ONBOARDINGWIDGET_H

#include "core/accountmanager.h"
#include "addaccountbutton.h"

#include <QMainWindow>
#include <QWidget>
#include <QVBoxLayout>
#include <QPushButton>
#include <QLabel>

class OnboardingWidget : public QWidget {
    Q_OBJECT

public:
    explicit OnboardingWidget(Michabo::AccountManager *manager, QWidget *parent = nullptr);
    virtual ~OnboardingWidget();

private:
    QVBoxLayout *m_layout;
    QLabel *m_michabo_icon;
    QLabel *m_welcome_label;
    QLabel *m_addaccount_label;
    AddAccountButton *m_addaccount_button;
    Michabo::AccountManager *m_account_manager;
};

#endif // ONBOARDINGWIDGET_H
