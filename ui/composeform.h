#ifndef COMPOSEFORM_H
#define COMPOSEFORM_H

#include <QWidget>
#include <QLineEdit>
#include <QTextEdit>
#include <QDialogButtonBox>
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QTextBlock>
#include <QComboBox>
#include <memory>

#include "core/accountmanager.h"
#include "attachmenteditorwidget.h"

class ComposeForm : public QWidget
{
    Q_OBJECT
public:
    explicit ComposeForm(Michabo::AccountManager *manager, QWidget *parent = nullptr);

    void setReplyTo(std::shared_ptr<Michabo::Post> p);

signals:

public slots:
    void updateScratch();
    void updateContentType();
    void uploadAttachment();

private:
    Michabo::Account *m_account;
    QLineEdit *m_subject;
    QTextEdit *m_content;
    QDialogButtonBox *m_button_box;
    QPushButton *m_send_button;
    QPushButton *m_cancel_button;
    QLabel *m_post_length_counter;
    QHBoxLayout *m_bottom_box;
    QVBoxLayout *m_layout;
    QString m_visibility;
    QComboBox *m_content_type;
    QPushButton *m_attach_button;
    AttachmentEditorWidget *m_attachment_editor;
    std::shared_ptr<Michabo::Post> m_reply_to;
    std::shared_ptr<Michabo::Post> m_scratch;
};

#endif // COMPOSEFORM_H
