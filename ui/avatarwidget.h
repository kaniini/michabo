#ifndef AVATARWIDGET_H
#define AVATARWIDGET_H

#include <QObject>
#include <QWidget>

class AvatarWidget : public QWidget
{
    Q_OBJECT
public:
    explicit AvatarWidget(QWidget *parent = nullptr);

    void setAvatar(const QImage &new_avi);
    QSize sizeHint() const override;

signals:

public slots:

protected:
    void paintEvent(QPaintEvent *event) override;

    QPixmap avatar;
};

#endif // AVATARWIDGET_H

