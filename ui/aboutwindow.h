#ifndef ABOUTWINDOW_H
#define ABOUTWINDOW_H

#include <QWidget>
#include <QDialog>
#include <QVBoxLayout>
#include <QLabel>

class AboutWindow : public QDialog
{
public:
    AboutWindow(QWidget * parent = nullptr);

private:
    QVBoxLayout *m_layout;
    QLabel *m_logo;
    QLabel *m_application_name;
    QLabel *m_copyright;
    QLabel *m_license;
};

#endif // ABOUTWINDOW_H
