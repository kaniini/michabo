#include "mainwindow.h"
#include "onboardingwindow.h"
#include "notificationimpl.h"
#include "core/account.h"
#include "core/accountmanager.h"
#include <QApplication>
#include <QDesktopServices>

#include <deque>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    // enable High DPI support for stock icons, etc, because Qt
    a.setAttribute (Qt::AA_UseHighDpiPixmaps);

    QCoreApplication::setOrganizationName("Pleroma");
    QCoreApplication::setOrganizationDomain("pleroma.social");
    QCoreApplication::setApplicationName("Michabo");
    QCoreApplication::setApplicationVersion("0.1");
    a.setWindowIcon(QIcon::fromTheme("michabo", QIcon(":/embedded-assets/icons/michabo-64.png")));

    Michabo::AccountManager manager;
    NotificationImpl notifications;
    QObject::connect(&manager, &Michabo::AccountManager::notification, &notifications, &NotificationImpl::showNotification);

    MainWindow mainwin (nullptr, &manager);
    OnboardingWindow onboardwin (nullptr, &manager, &mainwin);

    if (manager.hasAccounts())
        mainwin.show();
    else
        onboardwin.show();

    return a.exec();
}
