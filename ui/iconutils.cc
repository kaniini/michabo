#include "iconutils.h"

static QPixmap colorize_pixmap (QPixmap pixmap, QColor color) {
    QPixmap result (pixmap.width (), pixmap.height ());

    result.fill (color);
    result.setMask (pixmap.createMaskFromColor (Qt::black, Qt::MaskOutColor));

    return result;
}

static QColor desaturate (QColor c)
{
    int h, s, v;
    c.getHsv(&h, &s, &v);

    s /= 2;

    return QColor::fromHsv(h, s, v);
}

static QString icon_name_id (QString icon_name, QString fallback, IconState state){
    return icon_name +" :" + fallback + state;
}

QPixmap pixmap_from_theme (QPalette palette, QString icon_name, QString fallback, IconState state, int size)
{
    QIcon base = QIcon::fromTheme (icon_name, QIcon (fallback));
    QIcon derived = QIcon ();
    QString ico_key = icon_name_id (icon_name, fallback, state);
    QPixmap cached_pixmap;
    int supersize = size * 3;

    if (! QPixmapCache::find (ico_key, &cached_pixmap))
    {
        switch (state)
        {
        case Default:
            cached_pixmap = colorize_pixmap (base.pixmap (supersize, supersize),
                                                palette.text ().color ());
            break;

        case Highlighted:
            cached_pixmap = colorize_pixmap (base.pixmap (supersize, supersize),
                                                desaturate (palette.highlight ().color ()));
            break;

        case Enabled:
            cached_pixmap = colorize_pixmap (base.pixmap (supersize, supersize),
                                                palette.highlight ().color ());
            break;
        }

        QPixmapCache::insert (ico_key, cached_pixmap);
    }

    derived.addPixmap (cached_pixmap);
    return derived.pixmap (size, size);
}

IconState proper_icon_state (QRect r, QPoint p, bool enabled)
{
    // are we hovering over the icon?
    if (r.contains (p))
        return Highlighted;

    // is the underlying feature enabled?
    if (enabled)
        return Enabled;

    // return default
    return Default;
}
