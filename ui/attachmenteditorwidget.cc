#include "attachmenteditorwidget.h"
#include <QPainter>
#include <QStyledItemDelegate>

#define PREVIEW_SIZE        64

class PreviewDelegate : public QStyledItemDelegate
{
protected:
    void paint (QPainter * painter, const QStyleOptionViewItem & option, const QModelIndex & index) const;
    QSize sizeHint (const QStyleOptionViewItem & option, const QModelIndex & index) const;
};

void PreviewDelegate::paint(QPainter* painter, const QStyleOptionViewItem & option_, const QModelIndex &index) const
{
    QStyleOptionViewItem option = option_;
    initStyleOption (& option, index);

    auto alignment = Qt::AlignHCenter | Qt::AlignTop;
    QRect aligned_rect = QStyle::alignedRect(option.direction, alignment, QSize(PREVIEW_SIZE, PREVIEW_SIZE), option.rect);
    QPixmap preview = index.data(Qt::DecorationRole).value<QPixmap>();

    painter->save();
    painter->setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform, true);
    painter->drawPixmap(aligned_rect, preview, preview.rect());
    painter->restore();
}

QSize PreviewDelegate::sizeHint(const QStyleOptionViewItem &, const QModelIndex &) const
{
    return QSize(PREVIEW_SIZE, PREVIEW_SIZE);
}

AttachmentEditorModel::AttachmentEditorModel(std::shared_ptr<Michabo::Post> scratch)
    : m_scratch(scratch)
{
}

int AttachmentEditorModel::columnCount(const QModelIndex &) const
{
    return NColumns;
}

int AttachmentEditorModel::rowCount(const QModelIndex &) const
{
    return m_scratch->m_attachments.size ();
}

QVariant AttachmentEditorModel::data(const QModelIndex &index, int role) const
{
    int row = index.row ();
    int col = index.column ();
    auto att = m_scratch->m_attachments[row];

    if (role == Qt::DecorationRole && col == Preview)
        return att->m_preview;

    if (role != Qt::DisplayRole)
        return QVariant ();

    switch (col)
    {
    case Preview:
        break;

    case Description:
        return att->m_description;
    }

    return QVariant ();
}

QVariant AttachmentEditorModel::headerData(int section, Qt::Orientation, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant ();

    switch (section)
    {
    case Preview:
        return QString (tr("Preview"));
        break;

    case Description:
        return QString (tr("Description"));
        break;

    default:
        return QVariant ();
    }
}

Qt::ItemFlags AttachmentEditorModel::flags(const QModelIndex &index) const
{
    int col = index.column ();

    if (col != Description)
        return Qt::ItemIsEnabled;

    return Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable;
}

bool AttachmentEditorModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    int row = index.row ();
    int col = index.column ();

    if (role != Qt::EditRole)
        return false;

    if (col != Description)
        return false;

    auto att = m_scratch->m_attachments[row];
    att->setDescription (value.toString ());

    return true;
}

AttachmentEditorWidget::AttachmentEditorWidget(std::shared_ptr<Michabo::Post> scratch, QWidget *parent)
    : QTreeView(parent),
      m_model(new AttachmentEditorModel (scratch))
{
    setModel (m_model);
    setHeaderHidden (false);
    setRootIsDecorated (false);
    setVerticalScrollMode (QAbstractItemView::ScrollPerPixel);

    auto delegate = new PreviewDelegate;
    setItemDelegateForColumn (AttachmentEditorModel::Preview, delegate);
}

void AttachmentEditorWidget::updateDirty()
{
    m_model->layoutChanged();
}
