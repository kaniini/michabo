#include "notificationimpl.h"

#include <AppKit/AppKit.h>
#include <Foundation/Foundation.h>

void show_notification_osx (QString &topic, QString &message, QImage &avatar)
{
    qDebug() << "delivering notification";

    NSUserNotification *notification = [[[NSUserNotification alloc] init] autorelease];

    notification.title = topic.toNSString ();
    notification.informativeText = message.toNSString ();

    CGImageRef cg_img = avatar.toCGImage ();
    CGSize cg_size = avatar.size ().toCGSize ();

    NSImage *img = [[[NSImage alloc] initWithCGImage:cg_img size:cg_size] autorelease];
    notification.contentImage = img;

    [[NSUserNotificationCenter defaultUserNotificationCenter] deliverNotification: notification];
}
