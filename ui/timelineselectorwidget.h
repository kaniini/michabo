#ifndef TIMELINESELECTORWIDGET_H
#define TIMELINESELECTORWIDGET_H

#include <QWidget>
#include <QTabWidget>

#include "core/account.h"
#include "core/accountmanager.h"
#include "timelinewidget.h"

class TimelineSelectorWidget : public QTabWidget
{
public:
    TimelineSelectorWidget(Michabo::AccountManager *manager, QWidget * parent = nullptr);

private:
    Michabo::AccountManager *m_manager;
    Michabo::Account *m_account;
    TimelineModel *m_home_timeline;
    TimelineModel *m_public_timeline;
    TimelineModel *m_federated_timeline;
};

#endif // TIMELINESELECTORWIDGET_H
