# ![Michabo logo](https://git.pleroma.social/kaniini/michabo/raw/master/icons/michabo-64.png) Michabo

Michabo is a work in progress native (e.g. not Electron) desktop app
for Pleroma and Mastodon servers.

## Missing features

* Video support
* Profile editing
* Translated strings
* (Pleroma) Admin capabilities

However, if you don't need any of those things (you really do), Michabo
uses 1MB of RAM when not in heavy use.

It should also be noted that, while I wish for this project to exist under
the Pleroma umbrella, it has not yet any level of stability or usability
that would qualify it as an official project as yet.

## Building

You will need Qt headers.  On Debian, that is something like this:

    apt install 'qt5-*-dev' libqt5websockets5-dev

Next use qmake and make:

    qmake michabo.pro
    make -j8
    make install

## Running

Run the Michabo binary found at project root

    ./Michabo
